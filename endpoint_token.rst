.. _endpoint-token:

========================================
Token
========================================

The Token endpoint is a read/write resource that creates or returns a 
`user token` when the email and password are supplied.

Methods Supported
=================
POST

URL
===
``https://gt.viacycle.com/api/v1/token/?format=json``

Authentication
==============

Request Header
--------------

AuthKey: fhejr4rgf2_api_key_htr435ewdfhsef


Request Parameters (JSON)
=========================
#) email (required)
#) password (required)

Response Parameters (JSON)
==========================
#) email
#) token

Example (Raw)
=============
::

	-- request --
	POST https://dev2.viacycle.com/api/v1/token/?format=json
	AuthKey: 17080129_api_key_da47a51c0e34bc7
	Content-Type: application/json
	{"email": "abc@viacycle.com","password": "xxxxxxxxxx"}

	 -- response --
	200 OK
	Server:  nginx/1.0.5
	Date:  Thu, 14 Jun 2012 07:39:09 GMT
	Content-Type:  application/json; charset=utf-8
	Connection:  keep-alive
	Content-Length:  82
	{"email": "abc@viacycle.com", "token": "6850d0e_user_token_1f447d626352"}


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


