========================================
Welcome to viaCycle API
========================================

The viaCycle API provides access to viacycle data such as locations, 
number of available bikes and other data for developers.


Authentication / Authorization
==============================
An ``Api Key`` is required for access. We don't have an automated form yet, so 
please send us an email and we will send you the key 
(most likely in a few hours).

email info@viacycle.com with the following information:
	#) App Name (e.g. ``viaCycle@GT Mobile``)
	#) Programmatic Name (e.g. ``viacycle-gt-mobile``)
	#) Intended Platform

Once you have the Key, open endpoints (login not required) can be accessed by 
sending the ``Api Key`` as the ``AuthKey`` header. ::

	e.g. https://gt.viacycle.com/api/v1/openendpoint/

	Request Headers:
	AuthKey: fhejr4rgf2_api_key_htr435ewdfhsef


A ``User Token`` is required for closed endpoints (login required). 
User tokens are issued per user per app. A token is obtained by making an 
API call to the endpoint ``/token/``, with the following POST parameters:
	#) email
	#) password

Closed endpoints require the ``User Token`` to be sent as a header. ::

	e.g. https://gt.viacycle.com/api/v1/closedendpoint/

	Request Headers:
	AuthToken: ufh489r4_user_token_ewdh23823e233
  
Endpoints
=========

Open Endpoints (API Key Required)
----------------------------------
* :ref:`endpoint-location`
* :ref:`endpoint-token`

Closed Endpoints (User Token Required)
--------------------------------------
* coming soon

TOC
===

.. toctree::
   :maxdepth: 2

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



