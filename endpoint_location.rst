.. _endpoint-location:

========================================
Location
========================================

The Location endpoint is a read-only resource that returns a list of viaCycle
locations along with other information, including the current number of bikes 
available and information about those bikes.

Methods Supported
=================
GET

Example
=========
``https://gt.viacycle.com/api/v1/location/``

Authentication
==============

Request Header
--------------

AuthKey: fhejr4rgf2_api_key_htr435ewdfhsef


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`





